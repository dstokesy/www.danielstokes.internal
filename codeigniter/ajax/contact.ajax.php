<?php include_once '../application/core/Reviewer.php';

if($_POST){
    if($_POST['check'] == "")
    {
        $rsp = array();
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $error = "Please enter your email address";
        elseif($_POST['enquiry'] == "")
            $error = "Please enter an enquiry";
        else
        {
            $message = '
            <table>
                <tr>
                    <td>Email</td>
                    <td>' . $_POST['email'] . '</td>
                </tr>
                <tr>
                    <td>Enquiry</td>
                    <td>' . $_POST['enquiry'] . '</td>
                </tr>
            </table>
            ';
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            mail($_POST['email'], "Thankyou for your enquiry", $message,$headers);

            $rsp['response'] = 1;
            $rsp['msg'] = '<div class="success">Thankyou for you enquiry we will be in touch shortly</div>';

        }
        if(isset($error))
        {
            $rsp['response'] = 0;
            $rsp['msg'] = '<div class="error">' . $error . '</div>';
        }
        echo json_encode($rsp);
    }
}