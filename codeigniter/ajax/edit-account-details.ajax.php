<?php include_once '../application/core/Reviewer.php';

$login = new LoginSession();
$login->check_login();
$user = FrontEndUser::load($login->get_user_id());

if($_POST){
    $rsp = array();
    if($_POST['first_name'] == "")
        $error = "Please enter your first name";
    elseif($_POST['surname'] == "")
        $error = "Please enter your surname";
    elseif($_POST['security_question'] == "")
            $error = "Please select a security question";
    else
    {
        $form_data = array(
            'first_name'  => $_POST['first_name'],
            'surname'     => $_POST['surname']
        );
        dbRowUpdate('users', $form_data, 'id', $user->get_id());

        if($_POST['password'] != "")
        {
            if(strlen($_POST['password']) < 6 || strlen($_POST['password']) > 18)
                $error = "Your password needs to be between 6 and 18 characters";
            elseif($_POST['password'] != $_POST['confirm_password'])
                $error = "Passwords do not match";
            else
            {
                $form_data = array(
                    'password'    => md5($_POST['password'])
                );
                dbRowUpdate('users', $form_data, 'id', $user->get_id());
            }
        }
        if($_POST['security_answer'] != "")
        {
            $form_data = array(
                'security_question' => $_POST['security_question'],
                'security_answer'   => md5($_POST['security_answer'])
            );
            dbRowUpdate('users', $form_data, 'id', $user->get_id());
        }
        $rsp['response'] = 1;
        $rsp['msg'] = '<div class="success">User details updated</div>';
    }
    if(isset($error))
    {
        $rsp['response'] = 0;
        $rsp['msg'] = '<div class="error">' . $error . '</div>';
    }
    echo json_encode($rsp);
}