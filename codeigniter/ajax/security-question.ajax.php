<?php include_once '../application/core/Reviewer.php';

if($_POST){
    $rsp = array();

    if($_POST['answer'] == "")
        $error = "Please enter an answer";
    else
    {
        $result = dbRowSelectOne("SELECT * FROM users WHERE id = :id LIMIT 1", array(':id' => $_SESSION['temp_id']), "FETCH_ASSOC");
        if(isset($result['id']) && $result['security_answer'] == md5($_POST['answer']))
        {
            $login = new LoginSession();
            $login->login($result['id']);
            $user = FrontEndUser::load($login->get_user_id());
            $watchlist = $user->get_watch_list();
            $_SESSION['watchlist'] = $watchlist;
            $rsp['response'] = 1;
            $rsp['msg'] = '<div class="success">Redirecting</div>';
            $rsp['redirecturl'] = '/watch-list/';
        }
        else
        {
            $error = 'Incorrect';
        }
    }
    if(isset($error))
    {
        $rsp['response'] = 0;
        $rsp['msg'] = '<div class="error">' . $error . '</div>';
    }
    echo json_encode($rsp);
}