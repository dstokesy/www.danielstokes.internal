<?php include_once '../application/core/Reviewer.php';

$login = new LoginSession();
$login->check_login();
$user = FrontEndUser::load($login->get_user_id());
$watch_list = new WatchList();

$list = json_encode($watch_list->get_watch_list());

$form_data = array(
    'watch_list' => $list
);
dbRowUpdate('users', $form_data, 'id', $user->get_id());

header('Location: /watch-list/');