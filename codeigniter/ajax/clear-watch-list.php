<?php include_once '../application/core/Reviewer.php';

$login = new LoginSession();
$login->check_login();
$user = FrontEndUser::load($login->get_user_id());
$watch_list = new WatchList();

$watch_list->clear_watch_list();
header('Location: /watch-list/');