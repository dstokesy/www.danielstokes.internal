<?php include_once '../application/core/Reviewer.php';

if($_POST){
    if($_POST['check'] == "")
    {
        $rsp = array();
        /*$reCaptcha = new ReCaptcha("6Lezvv8SAAAAAEMcXkVc1SxZ0b4GC3UD3iwAtQtv");
        $reCaptcha_resp = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
        if($reCaptcha_resp->success != 1)
        {
            $error = "reCaptcha failed";
        }
        else
        {*/
            if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
                $error = "Please enter your email address";
            elseif($_POST['password'] == "")
                $error = "Please enter your password";
            else
            {
                $result = dbRowSelectOne("SELECT * FROM users WHERE email = :email LIMIT 1", array(':email' => $_POST['email']), "FETCH_ASSOC");
                if(isset($result['id']) && $result['password'] == md5($_POST['password']))
                {
                    $_SESSION['temp_id'] = $result['id'];
                    $rsp['response'] = 1;
                    $rsp['msg'] = '<div class="success">Redirecting</div>';
                    $rsp['redirecturl'] = '/security-question/';
                }
                else
                {
                    $error = 'Error please try again';
                }
            }
        //}
        if(isset($error))
        {
            $rsp['response'] = 0;
            $rsp['msg'] = '<div class="error">' . $error . '</div>';
        }
        echo json_encode($rsp);
    }
}