<?php include_once '../application/core/Reviewer.php';

if($_POST){
    if($_POST['check'] == "")
    {
        $rsp = array();
        if($_POST['first_name'] == "")
            $error = "Please enter your first name";
        if($_POST['surname'] == "")
            $error = "Please enter your surname";
        elseif(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $error = "Please enter your email address";
        elseif($_POST['password'] == "")
            $error = "Please enter your password";
        elseif(strlen($_POST['password']) < 6 || strlen($_POST['password']) > 18)
            $error = "Your password needs to be between 6 and 18 characters";
        elseif(!preg_match('/[A-Z]/', $_POST['password']))
            $error = "Password must contain at leaset 1 uppercase character";
        elseif(!preg_match('/[a-z]/', $_POST['password']))
            $error = "Password must contain at leaset 1 lowercase character";
        elseif($_POST['password'] != $_POST['confirm_password'])
            $error = "Passwords do not match";
        elseif($_POST['security_question'] == "")
            $error = "Please select a security question";
        elseif($_POST['security_answer'] == "")
            $error = "Please enter a security answer";
        else
        {
            $result = dbRowSelectOne("SELECT * FROM users WHERE email = :email LIMIT 1", array(':email' => $_POST['email']), "FETCH_ASSOC");
            if(!empty($result))
            {
                $error = "An unknown error has occured";
            }
            else
            {
                $form_data = array(
                    'first_name'        => $_POST['first_name'],
                    'surname'           => $_POST['surname'],
                    'email'             => $_POST['email'],
                    'password'          => md5($_POST['password']),
                    'security_question' => $_POST['security_question'],
                    'security_answer'   => md5($_POST['security_answer']),
                    'live'              => 1
                );
                dbRowInsert('users', $form_data);
                $rsp['response'] = 1;
                $rsp['msg'] = '<div class="success">Registration complete</div>';
            }
        }
        if(isset($error))
        {
            $rsp['response'] = 0;
            $rsp['msg'] = '<div class="error">' . $error . '</div>';
        }
        echo json_encode($rsp);
    }
}