<?php include_once '../../application/core/Reviewer.php';

if(isset($_GET['id']))
{
	//if they DID upload a file...
	if($_FILES['image']['name'])
	{
		$pos = strpos($_FILES['image']['name'],'php');
		if(!($pos === false)) {
			$error = 'error';
		}
		else
		{
			//Check file is a JPEG, PNG or GIF
			$file_types =  array('image/jpeg', 'image/gif', 'image/png');
			if(in_array($_FILES['image']['type'], $file_types))
			{
				if(!$_FILES['image']['error'])
				{
					$original_name = $_FILES['image']['name'];
					$original_name_parts = explode('.', $original_name);
					$new_file_name = $original_name_parts[0] . uniqid() . '.' . $original_name_parts[1];
					if($_FILES['image']['size'] > (1024000)) //can't be larger than 1 MB
					{
						$valid_file = false;
						$error = 'Oops!  Your file\'s size is to large.';
					}
					else
					{
						$valid_file = true;
					}
					
					//if the file has passed the test
					if(isset($valid_file))
					{
						//move it to where we want it to be
						move_uploaded_file($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/assets/' . $new_file_name);
						$success = 'Congratulations!  Your file was accepted.';
					}
				}
				//if there is an error...
				else
				{
					//set that to be the returned message
					$error = 'Ooops!  Your upload triggered the following error:  '.$_FILES['image']['error'];
				}
			}
			else
			{
				$error = 'File type not allowed';
			}
		}
	}

	if(isset($success))
	{
		$form_data = array(
		    'image'	=> $new_file_name
		);

		dbRowUpdate('shows', $form_data, 'id', $_GET['id']);

		header('Location: /admin/edit/image/?id=' . $_GET['id']);
	}
	else
	{
		header('Location: /admin/edit/image/?id=' . $_GET['id'] . (isset($error) ? '&error=' . $error : ''));
	}
}
