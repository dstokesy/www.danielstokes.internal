<?php include_once '../../application/core/Reviewer.php';

if($_POST){
    $rsp = array();
    if($_POST['name'] == "")
        $error = "Please enter a name";
    if($_POST['category'] == "")
        $error = "Please select a category";
    elseif($_POST['description'] == "")
        $error = "Please enter a description";
    elseif($_POST['score'] == "")
        $error = "Please enter a score";
    elseif($_POST['air_date_from'] == "")
        $error = "Please enter a start air date";
    elseif($_POST['air_date_to'] == "")
        $error = "Please enter an end air date";
    elseif(!isset($_POST['live']))
        $live = 0;
    else
    {
        $live = 1;
        $form_data = array(
            'id'            => $_POST['id'],  
            'name'          => $_POST['name'],
            'categories'    => $_POST['category'],
            'description'   => $_POST['description'],
            'score'         => $_POST['score'],
            'air_date_from' => $_POST['air_date_from'],
            'air_date_to'   => $_POST['air_date_to'],
            'live'          => $live
        );

        dbRowInsert('shows', $form_data);
        $rsp['response'] = 1;
        $rsp['msg'] = '<div class="success">Show Added</div>';   
    }
    if(isset($error))
    {
        $rsp['response'] = 0;
        $rsp['msg'] = '<div class="error">' . $error . '</div>';
    }
    echo json_encode($rsp);
}