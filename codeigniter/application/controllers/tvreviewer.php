<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tvreviewer extends CI_Controller {

	public function index($page = 'home')
	{
		if ( ! file_exists(APPPATH.'/views/tv-reviewer/'.$page.'.php'))
		{
			show_404();
		}

		$data['title'] = ucfirst($page);

		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('tv-reviewer/'.$page, $data);
		$this->load->view('footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/generic.php */