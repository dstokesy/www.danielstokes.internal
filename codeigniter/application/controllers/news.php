<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		/*if(!$this->session->userdata('logged_in'))
		{
			redirect('login', 'refresh');
		}*/

		$data['news'] = $this->news_model->get_news();
		$data['title'] = 'News archive';

		$this->load->view('header', $data);
		$this->load->view('navigation');
		$this->load->view('news/index', $data);
		$this->load->view('footer');
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('home', 'refresh');
	}

	public function view($slug)
	{


		$data['news_item'] = $this->news_model->get_news($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$data['title'] = $data['news_item']['title'];

		$this->load->view('header', $data);
		$this->load->view('navigation');
		$this->load->view('news/view', $data);
		$this->load->view('footer');
	}

	public function create()
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login', 'refresh');
		}

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Create a news item';

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('header', $data);
			$this->load->view('navigation');
			$this->load->view('news/create');
			$this->load->view('footer');

		}
		else
		{
			$this->news_model->set_news();
			$this->load->view('header', $data);
			$this->load->view('navigation');
			$this->load->view('news/success');
			$this->load->view('footer');
		}
	}

	public function delete($slug)
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login', 'refresh');
		}

		$data['news_item'] = $this->news_model->delete($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$this->load->view('header', $data);
		$this->load->view('navigation');
		$this->load->view('news/delete', $data);
		$this->load->view('footer');
	}

	public function edit($slug)
	{
		if(!$this->session->userdata('logged_in'))
		{
			redirect('login', 'refresh');
		}
		
		$data['news_item'] = $this->news_model->get_news($slug);

		if (empty($data['news_item']))
		{
			show_404();
		}

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Edit a news item';;

		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('header', $data);
			$this->load->view('navigation');
			$this->load->view('news/edit', $data);
			$this->load->view('footer');

		}
		else
		{
			$this->news_model->update_news($slug);
			$this->load->view('header', $data);
			$this->load->view('navigation');
			$this->load->view('news/update-success', $data);
			$this->load->view('footer');
		}
	}

}

/* End of file news.php */
/* Location: ./application/controllers/news.php */