<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shows extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('shows_model');
	}

	public function index()
	{
		$data['shows'] = $this->shows_model->get_shows();
		$data['title'] = 'TV Shows';

		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('tv-reviewer/shows', $data);
		$this->load->view('footer');
	}

	public function view($slug)
	{
		$data['show'] = $this->shows_model->get_shows($slug);

		if (empty($data['show']))
		{
			show_404();
		}

		$data['title'] = $data['show']['name'];

		$this->load->view('header');
		$this->load->view('navigation');
		$this->load->view('tv-reviewer/show', $data);
		$this->load->view('footer');
	}

}