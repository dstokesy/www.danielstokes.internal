<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
 
	function __construct()
	{
		parent::__construct();
		//$this->load->model('user','',TRUE);
		$this->load->model('user_model');
	}
 
	function index()
	{
		$this->session->set_userdata('logged_in', NULL);
		redirect('login', 'refresh');
	}
 
}

/* End of file news.php */
/* Location: ./application/controllers/logout.php */