<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LoginValidate extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        //$this->load->model('user','',TRUE);
        $this->load->model('user_model');
    }

    function index()
    {
        $this->load->library('form_validation');

        //Validate and apply sanitization to data so it can be run in db and runs check database method
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if($this->form_validation->run() == FALSE)
        {
            //If form validation isn't run load login page
            $this->load->view('header');
            $this->load->view('navigation');
            $this->load->view('login');
            $this->load->view('footer');
        }
        else
        {
            //Redirect to news page if login is successful
            redirect('news', 'refresh');
        }

    }

    function check_database($password)
    {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');

        //query the database
        $result = $this->user_model->login($username, $password);

        if($result)
        {
            $session = array();
            foreach($result as $row)
            {
                $session = array(
                'id' => $row->id,
                'username' => $row->username
                );
                $this->session->set_userdata('logged_in', $session);
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('check_database', 'Invalid username or password');
            return false;
        }
    }
}
?>