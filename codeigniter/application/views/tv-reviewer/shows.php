<section id="categories">
	<div class="section_heading category_filters controls">
		<ul class="filter-list">
			<li>Tv Shows :</li><?php
			?><li data-filter=".action" class="default_filter active"><a href="#" class="category_filter filter"><span>Action / Adventure</span></a></li><?php
			?><li data-filter=".animated"><a href="#" class="category_filter filter"><span>Animated</span></a></li><?php
			?><li data-filter=".comedy"><a href="#" class="category_filter filter"><span>Comedy</span></a></li><?php
			?><li data-filter=".drama"><a href="#" class="category_filter filter"><span>Drama</span></a></li><?php
			?><li data-filter=".fantasy"><a href="#" class="category_filter filter"><span>Fantasy</span></a></li><?php
			?><li data-filter=".horror"><a href="#" class="category_filter filter"><span>Horror</span></a></li><?php
			?><li data-filter=".non-fiction"><a href="#" class="category_filter filter"><span>Non-Fiction</span></a></li><?php
			?><li data-filter=".sci-fi"><a href="#" class="category_filter filter"><span>Sci-Fi</span></a></li>
		</ul>
	</div>
	<div class="section_content">
	    <div class="container">
	    	<div id="category_blocks" class="clearfix">
    			<p class="center">Below are multiple instances of the TV Show object</p>
    			<?php foreach ($shows as $show): ?>
			    	<div class="col-xs-6 col-md-4 mix <?=$show['categories']?>">
		    			<a href="/codeigniter/index.php/shows/<?=$show['slug']?>">
		    				<div class="category_block">
		    					<p class="h3"><?=$show['name']?></p>
		    				</div>
		    			</a>
			    	</div>
				<?php endforeach ?>
		    </div>
		</div>
	</div>
</section>