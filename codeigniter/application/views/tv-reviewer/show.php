<section id="show">
	<div class="section_heading">
		<h2><?=$title?></h2>
	</div>
	<div class="section_content">
		<div class="container">
			<div class="col-sm-6">
				<h3>Description</h3>
				<?=$show['description']?>
				<img src="/assets/<?=$show['image']?>" class="show_pic">
			</div>
			<div class="col-sm-6">
				<table class="show_table" width="100%">
					<tr>
						<th>Rating</th>
						<th>Air Date</th>
						<th>End Date</th>
					</tr>
					<tr>
						<td><?=$show['score']?></td>
						<td><?=$show['air_date_from']?></td>
						<td><?=$show['air_date_to']?></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</section>