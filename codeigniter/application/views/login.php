<section id="login">
	<div class="section_heading">
		<p class="h2">Login</p>
	</div>
	<div class="section_content">
		<div class="container">
			<div class="col-sm-4 col-sm-offset-4">
				<?php echo validation_errors(); ?>
				<?php echo form_open('loginvalidate'); ?>
				  <label for="username">Username:</label>
				  <input type="text" name="username"/>
				  <br/>
				  <label for="password">Password:</label>
				  <input type="password" name="password"/>
				  <br/>
				  <input type="submit" value="Login"/>
				</form>
			</div>
		</div>
	</div>
</section>