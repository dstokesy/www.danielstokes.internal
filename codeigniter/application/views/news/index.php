<section id="login">
	<div class="section_heading">
		<p class="h2">News</p>
	</div>
	<div class="section_content">
		<div class="container">
			<div class="col-sm-4 col-sm-offset-4">
				<?php foreach ($news as $news_item): ?>
				    <h2><?php echo $news_item['title'] ?></h2>
				    <div class="main">
				        <?php echo $news_item['text'] ?>
				    </div>
				    <p><a href="/codeigniter/index.php/news/<?=$news_item['slug']?>" class="btn">View article</a></p>
				    <?php /*<p><a href="/codeigniter/index.php/news-edit/<?=$news_item['slug']?>" class="btn">Edit article</a></p>
				    <p><a href="/codeigniter/index.php/news-delete/<?=$news_item['slug']?>" class="btn">Delete article</a></p>*/?>

				<?php endforeach ?>
				<?php /*<p><a href="/codeigniter/index.php/logout/" class="btn">Logout</a></p>*/?>
			</div>
		</div>
	</div>
</section>