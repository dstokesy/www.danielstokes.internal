<section id="login">
	<div class="section_heading">
		<h2>Create a news item</h2>
	</div>
	<div class="section_content">
		<div class="container">
			<div class="col-sm-4 col-sm-offset-4">
				<?php echo validation_errors(); ?>
				<?php echo form_open('news/create') ?>

					<label for="title">Title</label>
					<input type="input" name="title" /><br />

					<label for="text">Text</label>
					<textarea name="text"></textarea><br />

					<input type="submit" name="submit" value="Create news item" />

				</form>
			</div>
		</div>
	</div>
</section>




