<?php
class Shows_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function get_shows($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('shows');
			return $query->result_array();
		}

		$query = $this->db->get_where('shows', array('slug' => $slug));
		return $query->row_array();
	}

	public function view($slug)
	{
		$data['shows'] = $this->shows_model->get_shows($slug);
	}

}