<?php
class Front_end_user{

    public function __construct($data) {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }
	public static function load($id){
        $result = dbRowSelectOne("SELECT * FROM users WHERE id = :id LIMIT 1", array(':id' => $id), "FETCH_ASSOC");
        return new self($result);
	} 

	public static function load_all(){
		$data = dbRowSelect("SELECT * FROM users WHERE live = :live", array(':live' => 1), "FETCH_ASSOC");

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
	} 

    public function admin_check(){
        if($this->admin != 1)
        {
            header('Location: /welcome/');
        }
    }

    function get_id(){
        return $this->id;
    }

    function get_first_name(){
        return $this->first_name;
    }

    function get_surname(){
        return $this->surname;
    }

    function get_email(){
        return $this->email;
    }

    function get_password(){
        return $this->password;
    }

    function get_security_question(){
        return $this->security_question;
    }

    function get_watch_list(){
        return json_decode($this->watch_list, true);
    }

    function get_live(){
        return $this->live;
    }

}