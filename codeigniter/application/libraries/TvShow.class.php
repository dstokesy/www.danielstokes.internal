<?php
class TvShow {

    public function __construct($data) {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }

	public static function load($id){
        $result = dbRowSelectOne("SELECT * FROM shows WHERE id = :id LIMIT 1", array(':id' => $id), "FETCH_ASSOC");
        return new self($result);
	} 

	public static function load_all(){
		$data = dbRowSelect("SELECT * FROM shows WHERE live = :live", array(':live' => 1), "FETCH_ASSOC");

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
	}

    public static function load_category($category){
        $data = dbRowSelect("SELECT * FROM shows WHERE categories = :category", array(':category' => $category), "FETCH_ASSOC");
        
        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    } 

    public function get_id(){
        return $this->id;
    }

    public function get_name(){
        return $this->name;
    }

    public function get_categories(){
        return $this->categories;
    }

    public function get_description(){
        return '<p>' . $this->description . '</p>';
    }

    public function get_image(){
        if($this->image != '')
            return '<img src="/assets/' . $this->image . '" alt="' . $this->name . '" class="show_pic">';
    }

    public function get_review(){
        return $this->review;
    }

    public function get_score(){
        return $this->score . '/10';
    }

    public function get_cast(){
        return $this->cast;
    }

    public function get_air_date_from(){
        return $this->air_date_from;
    }

    public function get_air_date_to(){
        return $this->air_date_to;
    }

    public function get_related_shows(){
        return $this->related_shows;
    }

    public function get_live(){
        return $this->live;
    }
}