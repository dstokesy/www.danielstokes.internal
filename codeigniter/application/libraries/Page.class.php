<?php
class Page{

    public function __construct($data) {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }
	public static function load($id){
        $result = dbRowSelectOne("SELECT * FROM pages WHERE id = :id LIMIT 1", array(':id' => $id), "FETCH_ASSOC");
        return $result;
	} 

	public static function load_via_uri($uri){
		$result = dbRowSelectOne("SELECT * FROM pages WHERE uri = :uri LIMIT 1", array(':uri' => $uri), "FETCH_ASSOC");
        return $result;
	}

    public static function load_all(){
        $data = dbRowSelect("SELECT * FROM pages WHERE live = :live", array(':live' => 1), "FETCH_ASSOC");

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    }  

    function get_id(){
        return $this->id;
    }

    function get_name(){
        return $this->name;
    }

    function get_uri(){
        return $this->uri;
    }

    function get_template(){
        return $this->template;
    }

    function get_controller(){
        return $this->controller;
    }

    function get_hide_from_sitemap(){
        return $this->hide_from_sitemap;
    }

    function get_live(){
        return $this->live;
    }
}