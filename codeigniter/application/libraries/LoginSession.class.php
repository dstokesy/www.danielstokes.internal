<?php
class LoginSession
{
  private $logged_in=false;
  public $user_id;

  public function __construct($data = NULL) {
      if ($data)
      {
          foreach ($data as $key => $value)
          {
              $this->$key = $value;        
          }
      }
  }

  public function is_logged_in() {
    return $this->logged_in;
  }

  public function login($user) {
    if($user){
      $this->user_id = $user;
      $_SESSION['user_id'] = $user;
      $this->logged_in = true;
    }
  }

  public function logout() {
    unset($_SESSION['user_id']);
    unset($this->user_id);
    $this->logged_in = false;
  }

  public function check_login() {
    if(isset($_SESSION['user_id'])) {
      $this->user_id = $_SESSION['user_id'];
      $this->logged_in = true;
    } 
    else {
      header('Location: /');
    }
  }

  function get_user_id(){
    return $this->user_id;
  }

}