<?php
class WatchList {

	public $watchlist = array();

	public function __construct() {
		//If session isn't set load from db otherwise set watchlsit session as an array
		if(!isset($_SESSION['watchlist']))
		{
			$_SESSION['watchlist'] = array();
		}
	}

    public function get_watch_list() {
        if(!empty($_SESSION['watchlist']))
        {
            $this->watchlist = $_SESSION['watchlist'];
            return $this->watchlist;
        }
    }

    public function clear_watch_list() {
        unset($this->watchlist);
        unset($_SESSION['watchlist']);
    }

    public function set_watch_list_item($id) {
        if($id)
        {
        	$show = TvShow::load($id);
        	if(get_object_vars($show))
        	{
        		if(!in_array($id, $_SESSION['watchlist']))
        		{
        			$_SESSION['watchlist'][] = $id;
        		}
        	}
        }
    }

}