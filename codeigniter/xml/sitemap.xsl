<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>


<xsl:template match="/">
  <html>
  <body>
  <h2>Pages</h2>
    <table border="1">
      <tr bgcolor="#FDE57B">
        <th>Page ID</th>
        <th>Page Name</th>
      </tr>
      <xsl:for-each select="pages/page">
      <tr>
        <td><xsl:value-of select="page_id"/></td>
        <td><xsl:value-of select="page_name"/></td>
      </tr>
      </xsl:for-each>
    </table>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>