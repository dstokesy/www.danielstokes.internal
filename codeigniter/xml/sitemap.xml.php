<?php
require_once '/../hub.php';

header ("Content-Type:text/xml");

$pages = Page::load_all();

$_xml = '<?xml version="1.0"?>'; 
$_xml .="<pages>"; 
if($pages)
{
  foreach($pages as $page)
  {
    $_xml .='<page>'; 
    $_xml .='<page_id>' . $page->get_id() . '</page_id>'; 
    $_xml .='<page_name>' . $page->get_name() . '</page_name>'; 
    $_xml .='<page_uri>' . $page->get_uri() . '</page_uri>'; 
    $_xml .='<page_template>' . $page->get_template() . '</page_template>';
    $_xml .='<page_controller>' . $page->get_controller() . '</page_controller>';
    $_xml .='</page>'; 
  }
}
$_xml .='</pages>'; 

$xmlobj = new SimpleXMLElement($_xml);

print $xmlobj->asXML();