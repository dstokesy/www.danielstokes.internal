$(function(){

    //post via ajax
    $('.ajax_form').on('submit', function(e) {
        e.preventDefault();
        console.log('submit');
        $this = $(this);
        $.ajax({
            type: 'post',
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success : function (data) {
                if(data.msg)
                {
                    $responseEl = $this.find('.response');
                    console.log($responseEl);
                    $responseEl.hide().html(data.msg).slideDown();
                }
                if(data.redirecturl)
                {
                    window.location.replace(data.redirecturl);
                }
            }
        });
    });

    if(navigator.userAgent.toLowerCase().indexOf('firefox') <= -1)
    {
        //Attaches nicescroll plugin
        $("html").niceScroll({
            scrollspeed:        80,
            mousescrollstep:    35,
        });
    }

    //scroll down page on nav item click
    $('#primary_nav').on('click', '.on_page_link', function(e){
        e.preventDefault();
        $("html, body").animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 600);
    });

    $('#hero .rsContent').css('display', 'block');

    //attaches royalslider
    var slider =  $("#hero").royalSlider({
        transitionType: 'fade',
        loop: true,
        navigateByClick: false,
        autoPlay: {
            // autoplay options go gere
            enabled: true,
            pauseOnHover: false,
            delay: 5000
        }
    }).data('royalSlider');

    //attaches mixitup plugin
    $('#category_blocks').mixItUp({
        animation: {
          duration: 400,
          effects: 'fade translateZ(-360px) stagger(34ms)',
          easing: 'ease'
        },
        load: {
          filter: '.action'
        },
        animate: true,
        controls: {
          toggleLogic: 'and'
        }
    });

    var $filterOptions = $('.controls'),
        $btns = $filterOptions.children(),
        $allMix = $('.mix');

    var clickCoutner = 0;
    //On click of controls add/remove class
    $filterOptions.find("a").click(function(e) {
        e.preventDefault();
        var $this = $(this),
            $parent = $this.closest('li'),
            isActive = $parent.hasClass( 'active' ),
            filter;


        if($parent.hasClass('active')) {
            $('.filter-list li').removeClass('active');
            $('.default_filter').addClass('active');
            filter = '.action';
        }
        else {
            $('.filter-list li').removeClass('active');
            $parent.addClass('active');
            filter = $parent.data('filter');
        }
        //runs the filter
        $('#category_blocks').mixItUp('multiMix', {
            filter:  filter
        });
    });

    //HTML5 Fullscreen api
    function toggleFullScreen() {
        //If not in fullscreen mode - enter fullscreen mode for each browser type
        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {
            if (document.documentElement.requestFullscreen) {
                document.documentElement.requestFullscreen();
            }
            else if (document.documentElement.msRequestFullscreen) {
                document.documentElement.msRequestFullscreen();
            }
            else if (document.documentElement.mozRequestFullScreen) {
                document.documentElement.mozRequestFullScreen();
            }
            else if (document.documentElement.webkitRequestFullscreen) {
                document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
        }
        // Otherwise if in fullscreen mode - exit full screen mode for each browser type
        else {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            }
            else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            }
            else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            }
            else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
        }
    }
    
    $('.fullscreen').on('click', function(e){
        e.preventDefault();
        toggleFullScreen();
    });

});