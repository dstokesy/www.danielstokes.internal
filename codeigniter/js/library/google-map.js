$(function(){

    // Center of the map
    var latLngLeedsMet = new google.maps.LatLng(53.82743,-1.59325);
    var latLngCentre = new google.maps.LatLng(53.83128,-1.59342);

    // Create the map
    var map = new google.maps.Map(document.getElementById("map-canvas"), {
        zoom              : 14,
        center            : latLngCentre,
        disableDefaultUI  : true,
        zoomControl       : true,
        mapTypeId         : google.maps.MapTypeId.ROADMAP,
        scrollwheel       : false
        //styles            : mapStyles
    });

    // Create the Barrowford marker
    var PdMarkerTvReviewer = new google.maps.Marker({
        map           : map,
        position      : latLngLeedsMet,
        animation     : google.maps.Animation.DROP,
        icon          : '/graphics/map-canvas/map-pin.png'
    });

    // Bind info window objects to click events
    google.maps.event.addListener(PdMarkerTvReviewer, 'click', function() {
         infowindowLeedsMet.open(map,PdMarkerTvReviewer);
    });

    //if map position changes recenter to map pin
    google.maps.event.addListener(map, 'center_changed', function() {
        window.setTimeout(function() {
          map.panTo(PdMarkerTvReviewer.getPosition());
        }, 3000);
    });

});