$(function() {
	
	// Determine whether the browser supports overflow-scrolling
	function supportsOverflowScrolling() {
		return !($('html').hasClass('no-overflowscrolling'));
	}
	
	// Determine whether the popout is currently active
	function isPopoutActive() {
		return ($('body').hasClass('popout_left') || $('body').hasClass('popout_right'));
	}
	
	// Close the active popout window
	function closeActivePopout() {
		$('body').removeClass('popout_left popout_right');
		$('body').addClass('close_popout_left close_popout_right');
	}
	
	// Open a popout window
	function openPopout(which) {
		$('body').addClass('popout_' + which);
		
		// No overflow support? Set a fixed height and scroll page to the top
		if (! supportsOverflowScrolling())
		{
			$('#popout_' + which).height($(document).height());
			window.scrollTo(0, 1);
		}
	}

	// Left button events
	$('#mobile_header_button_right').on('touchstart click', function(e) {
		e.stopPropagation();		
		e.preventDefault();

		(! isPopoutActive()) ? openPopout('right') : closeActivePopout();
	});
	
	/*
	// Right button events
	$(document).on('click', '#mobile_header_button_right', function(e) {
		e.stopPropagation();		
		e.preventDefault();

		(! isPopoutActive()) ? openPopout('right') : closeActivePopout();
	});
	*/

	// If someone touches outside the popouts while one of them is active, then cancel
	// the default action and close the popout.
	$('body').on('touchstart', function(e) {
		
		if (isPopoutActive() && ! $(e.target).closest('#popout_left, #popout_right').length)
		{
			e.stopPropagation();
			e.preventDefault();
			closeActivePopout();
		}
	});

	// Ensure the popout menus handle their own touch events
	$('#popout_left, #popout_right').on('touchmove touchstart', function(e) {
		
		var ele = this,
			$ele = $(ele);
				
		// Ensure we don't scroll the page outside of the popout
		if (e.type == "touchstart")
		{
			if (ele.scrollTop === 0) {
				ele.scrollTop = 1;
			} else if (ele.scrollHeight === ele.scrollTop + ele.offsetHeight) {
				ele.scrollTop -= 1;
			}
		}
		
		if (e.type == "touchmove")
		{
			// No scrollbar? Then preventdefault to ensure we don't scroll outside
			if ($ele.get(0).scrollHeight < $ele.innerHeight())
				e.preventDefault();
		}
	});

	$(document).on('click', '.open_contact_box', function(e) {
		e.preventDefault();
		closeActivePopout();
		openTopBlock();

	});

	//slide page using navigation after mobile nav closes
	$("#mobile_nav .on_page_link").on("click", "a", function(e) {
	    e.preventDefault();
	    closeActivePopout();
	    $this = $(this);
	    setTimeout(function(){
		    $("html, body").animate({
		      scrollTop: $( $this.attr('href') ).offset().top
		    }, 600);
		}, 800);
	});

	//Opens news popup after mobile nav closes
	if($(window).width() <= 767)
	{
		$(".popup-news").on('click', function (e) {
		  	e.preventDefault();
		  	closeActivePopout();
			$.magnificPopup.open({
				items : {src : $(this).attr('href')},
				type : "ajax",
				focus : 'input:first',
				removalDelay: 500,
				closeOnBgClick : false,
				mainClass: 'mfp-fade',
				callbacks: {
				  open: function() {
				    timer = setTimeout(function () {
				        window.location = "https://www.facebook.com/rejuvenate.productions";
				    }, 5000);
				  },
				  close: function() {
				    clearTimeout(timer);
				  }
				}
			});
		});
	}

	//Slide down contact form
	$(".contact_trigger").on("click", function(e){
		e.preventDefault();
		closeActivePopout();
	});
	
});