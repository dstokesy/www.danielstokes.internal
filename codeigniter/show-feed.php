<?php
require_once 'hub.php';

header('Content-Type: application/json');
if(isset($_GET['show']))
{
	$show = TvShow::load($_GET['show']);
	echo json_encode($show);
}
else if(isset($_GET['category']))
{
	$category = TvShow::load_category($_GET['category']);
	echo json_encode($category);
}
else
{
	$shows = TvShow::load_all();
	echo json_encode($shows);
}