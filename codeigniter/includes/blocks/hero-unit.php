<div class="royalSlider" id="hero">
    <div class="rsContent">
        <img src="/graphics/backgrounds/tv-test.jpg" class="rsImg" alt="Hero image">
        <div class="rsAblok hero_content">
            <div class="center">
                <h1>TV Info &amp; Reviews</h1>
                <h3>Ut lectus porta turpis eros mauris</h3>
            </div>
        </div>
    </div>
    <div class="rsContent">
        <img src="/graphics/backgrounds/bg1.jpeg" class="rsImg"  alt="Hero image">
        <div class="rsAblock hero_content">
            <div class="center">
                <h1>How I Met Your Mother</h1>
                <h3>Ut lectus porta turpis eros mauris</h3>
            </div>
        </div>
    </div>
</div>
<div id="hero_ghost"></div>