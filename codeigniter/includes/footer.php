<footer>
	<div id="footer">
	    <div class="container">
			<div class="col-xs-12">
				<p>&copy; Stokes <?=date('Y')?></p>
				<nav>
					<ul id="footer_nav">
						<li><a href="#">Terms and Conditions</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Cookie Policy</a></li>
						<li><a href="#">Sitemap</a></li>
					</ul>
				</nav>
			</div>
	    </div>
	</div>
</footer>

