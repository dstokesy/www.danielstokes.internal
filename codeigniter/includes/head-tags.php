<meta charset="utf-8">
<title>Case Catalogue</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700,300' rel='stylesheet' type='text/css'>

<?php
echo_css($css = array(
        '/bootstrap/bootstrap.min.css',
        '/layout.less.css',
        '/forms.less.css',
        '/buttons.less.css',
        '/icons.less.css',
        '/library/animate.less.css',
        '/plugins/jquery.royalslider.css'
    )
);

echo_js($js = array(
		'/modernizr.js'
	)
);
?>
<!--[if lt IE 9]>
<script type="text/javascript" src="/holding/js/respond.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!--><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script><!--<![endif]-->