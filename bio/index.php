<?php require_once '../hub.php' ?>
<?php include INCLUDES . 'doctype.php'?>
<head>
<?php include INCLUDES . 'head-tags.php'?>
</head>
<body>
<div id="wrapper">
	<?php include INCLUDES . 'header.php'?>
	<main>
		<section id="hero-unit">
			<div class="content">
				<div id="hero-content">
					<img src="/graphics/profile.jpg" alt="Daniel Stokes" class="profile wow animated fadeInDownBig">
					<h1 class="wow animated fadeInDownBig">Daniel Stokes</h1>
					<h3 class="wow animated fadeInUpBig">All About Me</h3>
					<p class="social-icons wow animated fadeInUpBig"><a href="#" class="contact-scroll"><span class="icon icon-email">@</span></a><a href="http://uk.linkedin.com/pub/daniel-stokes/a1/100/786/en" target="_blank"><span class="icon icon-linkedin"></span></a></p>
				</div>
			</div>
		</section>
		<section id="what-do-i-do" class="content-section">
			<div class="container">
				<h3>What Do I Know?</h3>
				<h4>Below are lists showing what I am skilled and experienced in.</h4>
				<div class="clearfix">
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInLeft">
							<span class="icon icon-php"></span>
							<p class="title">PHP</p>
							<ul>
								<li>Using model view controllers and bespoke content management systems.</li>
								<li>Using object-orientated PHP and data sanitization techniques.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInRight">
							<span class="icon icon-html5"></span>
							<p class="title">HTML</p>
							<ul>
								<li>Using correct HTML5 elements and developing SEO friendly mark-up.</li>
								<li>Building HTML emails</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInLeft">
							<span class="icon icon-css3"></span>
							<p class="title">CSS</p>
							<ul>
								<li>Styling responsive websites efficiently.</li>
								<li>Using pre-processors such as LESS and SASS</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInRight">
							<span class="icon icon-javascript"></span>
							<p class="title">JavaScript</p>
							<ul>
								<li>Using plugins and API&apos;s</li>
								<li>Finding creative solutions to bugs and problems</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInLeft">
							<span class="icon icon-mobile"></span>
							<p class="title">Responsive</p>
							<ul>
								<li>Developing responsive websites for a wide variety of tablet and mobile devices.</li>
								<li>Creating a variety of types of mobile navigations.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInRight">
							<span class="icon icon-share2"></span>
							<p class="title">Social Media</p>
							<ul>
								<li>Using API's to load data.</li>
								<li>Using social media to aid SEO.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInLeft">
							<span class="icon icon-users"></span>
							<p class="title">Teamwork</p>
							<ul>
								<li>Working with multiple developers on and array of projects.</li>
								<li>Leading university projects to consistently high levels.</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="skill wow animated fadeInRight">
							<span class="icon icon-comments"></span>
							<p class="title">Communication</p>
							<ul>
								<li>Discussing creative ideas with clients.</li>
								<li>Contributing to team meetings.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="cta col-xs-12">
					<p class="h3">Like what you see?</p>
					<p><a href="#" class="btn contact-scroll">Get in touch!</a></p>
				</div>
			</div>
		</section>
		<section id="what-do-i-know" class="content-section blue">
			<div class="container">
				<h3>My Experience</h3>
				<div class="col-sm-5">
					<p class="h4">School</p>
					<p>My first experience of website development was in school. The website was built in dreamweaver and was built in tables.</p>
					<p class="h4">College</p>
					<p>I first started to learn how to build websites in code at college. Whilst there I learnt the basics of HTML, CSS and PHP.</p>
					<p class="h4">University</p>
					<p>At university I started to expand my knowledge of website development such as object orientated PHP and jQuery. Along with this I started to learn techniques for how websites are developed in the real world.</p>
				</div>
				<div class="col-sm-7">
					<p class="h4">Rejuvenate Productions Ltd</p>
					<p>My first job as a website developer was at a company called <a href="https://www.rejuvenateproductions.com/" target="_blank">Rejuvenate Productions Ltd</a> as a student placement intern and then afterwards as a full time job. This is where I learn vast amount of knowledge about how to build websites in a professional enviroment.</p>
					<p>To begin with I was working on adding existing functionality to websites so I could gain experience in a wide variety of different websites which were all built bespokely. Along with this I build HTML emails and tested these in a wide variety of email clients.</p>
					<p>From this I progressed into being given my own content managed responisve websites of a variety of different sizes and different types of clients.</p>
				</div>
				<div class="cta col-xs-12">
					<p class="h3">Want to know more?</p>
					<p><a href="#" class="btn white contact-scroll">Get in touch!</a></p>
				</div>
			</div>
		</section>
		<section id="contact" class="content-section">
			<div class="container">
				<h3>Get In Touch</h3>
				<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
					<form method="post" action="/ajax/contact.ajax.php" class="ajax-form clearfix wow animated fadeInDown" novalidate>
						<div class="response"></div>
						<input type="hidden" name="check">
						<div class="form-group">
							<input type="text" name="name" class="form-control" required>
							<label placeholder="Name"></label>
						</div>
						<div class="form-group">
							<input type="text" name="email" class="form-control" required>
							<label placeholder="Email"></label>
						</div>
						<div class="form-group">
							<textarea name="message" class="form-control" required></textarea>
							<label placeholder="Message"></label>
						</div>
						<div class="form-group">
							<button type="submit" class="btn">Send</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</main>
</div>
<?php include INCLUDES . 'footer-tags.php'?>
</body>
</html>