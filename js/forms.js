$(function(){
    //post via ajax
    $('.ajax-form').on('submit', function(e) {
        e.preventDefault();
        $this = $(this);
        $.ajax({
            type: 'post',
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success : function (data) {
                if(data.msg)
                {
                    $responseEl = $this.find('.response');
                    $responseEl.hide().html(data.msg).slideDown();
                }
                if(data.redirecturl)
                {
                    window.location.replace(data.redirecturl);
                }
            }
        });
    });
});