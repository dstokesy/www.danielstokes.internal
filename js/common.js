$(function(){

        wow = new WOW({
            animateClass : 'animated',
            offset       : 100,
            mobile       : false
        });
        wow.init();

        $('#navigation-trigger').on('click', function(e){
            e.preventDefault();
            if(!$('body').hasClass('navigation-open'))
            {
                $('body').addClass('navigation-open');
                $('#navigation').find('li').addClass('animated slideInDownSmall');
            }
            else
            {
                $('body').removeClass('navigation-open');
                $('#navigation').find('li').removeClass('animated slideInDownSmall'); 
            }
        });

        $(document).on('click', '.contact-scroll', function(e){
            e.preventDefault();
            if($('body').hasClass('navigation-open'))
            {
                $('body').removeClass('navigation-open');
            }
            $("html, body").animate({
                scrollTop: $('#contact').offset().top
            }, 400);
        });

        $('.project-detail-trigger').on('click', function(e){
            e.preventDefault();
            $this = $(this);
            $('#project-detail').fadeOut(400, function(){
                $('#project-detail').load('/ajax/project-detail.ajax.php?project=' + $this.data('project'), function(){
                    $('#project-detail').fadeIn(400, function(){
                        $("html, body").animate({
                            scrollTop: $('#project-detail').offset().top -80
                        }, 400);
                    });
                });
                
            });
        });

        $(document).on('click', '.hide-project-detail', function(e){
            e.preventDefault();
            $('#project-detail').fadeOut();
        });

});