$(function() {
	
	// Determine whether the browser supports overflow-scrolling
	function supportsOverflowScrolling() {
		return !($('html').hasClass('no-overflowscrolling'));
	}
	
	// Determine whether the popout is currently active
	function isPopoutActive(popout) {
		return ($('body').hasClass('popout_' + popout));
	}
	
	// Close the active popout window
	function closeActivePopout(popout) {
		$('body').removeClass('popout_' + popout);
		$('body').addClass('close_popout_' + popout);
	}
	
	// Open a popout window
	function openPopout(popout) {
		$('body').addClass('popout_' + popout);
		$('#navigation').find('li').addClass('animated');
		// No overflow support? Set a fixed height and scroll page to the top
		if (! supportsOverflowScrolling())
		{
			$('#popout_' + which).height($(document).height());
			window.scrollTo(0, 1);
		}
	}

	// Left button events
	$('#nav_trigger_left').on('click', function(e) {
		e.stopPropagation();		
		e.preventDefault();

		(! isPopoutActive('left')) ? openPopout('left') : closeActivePopout('left');
	});

	// If someone touches outside the popouts while one of them is active, then cancel
	// the default action and close the popout.
	$('body').on('touchstart', function(e) {
		
		if (isPopoutActive() && ! $(e.target).closest('#popout_left').length)
		{
			e.stopPropagation();
			e.preventDefault();
			closeActivePopout();
		}
	});

	// Ensure the popout menus handle their own touch events
	$('#popout_left').on('touchmove touchstart', function(e) {
		
		var ele = this,
			$ele = $(ele);
				
		// Ensure we don't scroll the page outside of the popout
		if (e.type == "touchstart")
		{
			if (ele.scrollTop === 0) {
				ele.scrollTop = 1;
			} else if (ele.scrollHeight === ele.scrollTop + ele.offsetHeight) {
				ele.scrollTop -= 1;
			}
		}
		
		if (e.type == "touchmove")
		{
			// No scrollbar? Then preventdefault to ensure we don't scroll outside
			if ($ele.get(0).scrollHeight < $ele.innerHeight())
				e.preventDefault();
		}
	});
	
});