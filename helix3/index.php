<?php include_once '../hub.php';
include INCLUDES . '/helix3/doctype.php'?>
<head>
<?php include INCLUDES . '/helix3/head-tags.php'?>
</head>
<body>
<div id="wrapper">
	<div id="main_content_wrapper">
		<main class="clearfix">
			<div class="site_third yellow_bg">
				<h1>LinEn</h1>
				<h3>Click on a link below to find out more</h3>
				<div class="tab_container">
					<div class="tab_links">
						<p class="icon_container"><a href="#linenInfo" class="tab_link active"><span class="icon icon-info"></span>Info</a><a href="#linenOverview" class="tab_link"><span class="icon icon-text"></span>Overview</a><a href="#linenDownload" class="tab_link"><span class="icon icon-download"></span>Download</a><a href="#linenInsturctions" class="tab_link"><span class="icon icon-clipboard"></span>Instructions</a></p>
					</div>
					<div class="tabs">
						<div id="linenInfo" class="active tab">
							<h3>Information</h3>
							<p>The user guide for this version of LinEn is for using the tool within Helix3 2009R1. LinEn (Linux Encase Acquisition Utility) is an acquisition tool by Guidance Software. It can be found in Helix3 when running the Helix3 disk as a live disk. LinEn has the ability to acquire a disk and obtain the hash value of a disk; this user guide will go through each of these processes step by step.</p>
						</div>
						<div id="linenDownload" class="tab">
							<h3>Download</h3>
							<p>As LinEn is a part of Helix3 to download the tool Helix3 must be downloaded. This can be done from the manufacturers website found <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" target="_blank">here</a>. Downloading the tool is very simple; as it is a free tool all that is required is to add Helix3 to the basket and follow the checkout journey. Once this has been completed; the manufacturer E-Fense will then send a link so the file Helix3 can be downloaded. The file to download is an ISO file. Once downloaded this can then be burnt to a disk using the disk burner found in Windows. No further steps are required to be taken to install Helix3.</p>
						</div>
						<div id="linenInsturctions" class="tab">
							<h3>Instructions</h3>
							<p>LinEn can be found on Helix3 when running it as a live disk. To do this the following steps have to be taken.</p>
							<ol>
								<li>Insert the Helix3 disk that has been created (see download section to see how to create a Helix3 disk).</li>
								<li>Open up the computers boot menu and boot the computer from the optical drive.</li>
								<li>Helix3 will then load its boot menu as can be seen in figure 1 <img src="/graphics/helix3/boot-menu.JPG" class="img"><small>Figure 1 - Helix3 boot menu</small></li>
								<li>Helix3 will then begin to boot as a live disk as can be seen in figure 2 <img src="/graphics/helix3/booting.JPG" class="img"><small>Figure 2 - Helix3 booting as a live disk</small></li>
								<li>Once booted Helix3 GUI is visible. Helix3 when booted as a live disk uses a modified version of Ubuntu; from within this LinEn is stored.</li>
								<li>To open LinEn click on the Applications button in the top navigation bar on the screen, then select Forensics &amp; IR and then LinEn as shown in Figure 3.<img src="/graphics/helix3/main.JPG" class="img"><small>Figure 3 - Location of LinEn within Helix3</small></li>
							</ol>
							<p>The following instructions show how to obtain the hash value of a drive.</p>
							<ol>
								<li>When opened LinEn opens like figure 4 showing a lists of drives able to acquire or hash and details of these drives.<img src="/graphics/linen/main.JPG" class="img"><small>Figure 4 - LinEn</small></li>
								<li>To obtain the hash value of a drive the hash option needs to be selected in LinEn.</li>
								<li>Once this has been done the drive that is going to be hashed needs to be selected as shown in figure 5.<img src="/graphics/linen/drive-choice.JPG" class="img"><small>Figure 5 - LinEn hashing drive choice</small></li>
								<li>One the drive has been selected the start and end sector of the drive need to be selected; by narrowing down the start and end sectors the drive will be hashed much quicker.</li>
								<li>Next the type of hash needs to be selected, in LinEn MD5 and Sha1 can be chosen from or both can be run is preferred.</li>
								<li>Once these options have been selected LinEn will then hash the file and display the results on the screen and provide the option of saving the results to a text file as shown in figures 6 &amp; 7.<img src="/graphics/linen/hash-results.JPG" class="img"><small>Figure 6 - LinEn has results</small><img src="/graphics/linen/results-file.JPG" class="img"><small>Figure 7 - LinEn - Hash results written to a .txt file</small></li>
							</ol>
							<p>The following instructions show how acquire a device.</p>
							<ol>
								<li>To acquire a device in LinEn the acquisition option need to be selected.</li>
								<li>Similar to the hashing option when acquiring a device the device that needs to be acquired needs to be selected.</li>
								<li>Next the location and name of the image needs to be entered.</li>
								<li>Next case information needs to be entered such as the case number, examiner name, evidence number, description, current date and time and any notes the user wishes to enter.</li>
								<li>The next option that needs to be chosen is whether to use compression when acquiring a device, if compression is chosen the image file will be smaller but the acquisition will take much longer.</li>
								<li>The next options to choose from are what hash type the file will be acquired in, if the image file needs to be password protected, and like the hashing functionality in LinEn the start and end sectors of the drive.</li>
								<li>The final options that need to be selected are the size of each image file (multiple files are created which are read as one file when the image is analysed). Also the size of sectors needs to be chosen, this must be a power of 2 and less than 512. Finally the error granularity sectors need to be select and again this must be to the power of 2 but less than the sector size previously chosen.</li>
								<li>Once all of the options have been selected LinEN will being to acquire the drive.<img src="/graphics/linen/acquiring.JPG" class="img"><small>Figure 8 - LinEn acquiring a drive</small></li>
								<li>The image files will then be written to the selected location as shown in figure 9.<img src="/graphics/linen/acquisition-results.JPG" class="img"></li>
							</ol>
							<p>As an internet connection is not possible in Helix3 the server option can't be used in LinEn so this concludes the LinEn user guide.</p>
						</div>
						<div id="linenOverview" class="tab">
							<h3>Overview</h3>
							<p>Manufacturer - E-Fense</p>
							<p>Version - 2009R1</p>
							<p>Download link - <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" target="_blank">www.e-fense.com/store/index.php?_a=viewProd&productId=11</a></p>
							<p>OS - Runs as a live disk (Ubuntu)</p>
							<p>Files used - connected drives that have been read by the operating system</p>
						</div>
					</div>
				</div>
			</div>
			<div class="site_third black_bg">
				<h1>USBDeview</h1>
				<h3>Click on a link below to find out more</h3>
				<div class="tab_container">
					<div class="tab_links">
						<p class="icon_container"><a href="#usbDeviewInfo" class="tab_link active"><span class="icon icon-info"></span>Info</a><a href="#usbDeviewOverview" class="tab_link"><span class="icon icon-text"></span>Overview</a><a href="#usbDeviewDownload" class="tab_link"><span class="icon icon-download"></span>Download</a><a href="#usbDeviewInsturctions" class="tab_link"><span class="icon icon-clipboard"></span>Instructions</a></p>
					</div>
					<div class="tabs">
						<div id="usbDeviewInfo" class="active tab">
							<h3>Information</h3>
							<p>The user guide for this version of USBDeview is for using the tool within Helix3 2009R1. USBDeview is a tool that uses registry files to list all currently and previously connected USB devices. USBDeview can be found in Helix3 when running the Helix3 disk in Windows.</p>
						</div>
						<div id="usbDeviewDownload" class="tab">
							<h3>Download</h3>
							<p>As USBDeview is a part of Helix3 to download the tool Helix3 must be downloaded. This can be done from the manufacturers website found <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" target="_blank">here</a>. Downloading the tool is very simple; as it is a free tool all that is required is to add Helix3 to the basket and follow the checkout journey. Once this has been completed; the manufacturer E-Fense will then send a link so the file Helix3 can be downloaded. The file to download is an ISO file. Once downloaded this can then be burnt to a disk using the disk burner found in Windows. No further steps are required to be taken to install Helix3.</p>
						</div>
						<div id="usbDeviewInsturctions" class="tab">
							<h3>Instructions</h3>
							<p>USBDeview can be found on Helix3 when running it in Windows 7. Helix3 can be booted in Windows 7 by inserting the disk and either automatically booting or running helix.exe.</p>
							<ol>
								<li>USBDeview can be found in Helix3 in page 3 of the Incident Response tab as shown in figure 1.<img src="/graphics/helix3/usbdeview.jpg" class="img"><small>Figure 1 - USBDeview in Helix3</small></li>
								<li>Once opened USBDeview automatically opens the live systems registry file so any previously or currently connected USB devices are shown.<img src="/graphics/usbdeview/usbdeview.jpg" class="img"><small>Figure 2 - USBDeview on load</small></li>
								<li>Each device has a list of properties; these can be viewed in the table of results or by clicking on the USB device and then file and properties.<img src="/graphics/usbdeview/properties.jpg" class="img"><small>Figure 3 - USBDeview - USB device properties</small></li>
								<li>Devices can also be filtered by toggling disconnected devices, devices without a serial number/port, devices without drivers and USB hubs.</li>
								<li>USB Deview can also be used to execute a command when a device is connected by opening the advanced options.<img src="/graphics/usbdeview/advanced-options.jpg" class="img"><small>Figure 4 - USB Deview's advanced options</small></li>
							</ol>
							<p>The latest version of USBDeview has the ability to open up an exported registry file; however the version of Helix3 doesn't have this functionality. This concludes the USBDeview user guide.</p>
						</div>
						<div id="usbDeviewOverview" class="tab">
							<h3>Overview</h3>
							<p>Manufacturer - Nirsoft</p>
							<p>Download link - <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" target="_blank">www.e-fense.com/store/index.php?_a=viewProd&productId=11</a></p>
							<p>OS - Windows 2000, XP, 2003, Vista, Server 2008, 7, and 8.</p>
							<p>Files used - Windows registry files</p>
						</div>
					</div>
				</div>
			</div>
			<div class="site_third yellow_bg">
				<h1>PC On/Off Time</h1>
				<h3>Click on a link below to find out more</h3>
				<div class="tab_container">
					<div class="tab_links">
						<p class="icon_container"><a href="#pcOnOffTimeInfo" class="tab_link active"><span class="icon icon-info"></span>Info</a><a href="#pcOnOffTimeOverview" class="tab_link"><span class="icon icon-text"></span>Overview</a><a href="#pcOnOffTimeDownload" class="tab_link"><span class="icon icon-download"></span>Download</a><a href="#pcOnOffTimeInsturctions" class="tab_link"><span class="icon icon-clipboard"></span>Instructions</a></p>
					</div>
					<div class="tabs">
						<div id="pcOnOffTimeInfo" class="active tab">
							<h3>Information</h3>
							<p>The user guide for this version of PC On/Off Time is for using the tool within Helix3 2009R1. PC On/Off Time displays the on and off times on a computer. PC On/Off Time can be found in Helix3 when running Helix3 in Windows.</p>
						</div>
						<div id="pcOnOffTimeDownload" class="tab">
							<h3>Download</h3>
							<p>As PC On/Off Time is a part of Helix3 to download the tool Helix3 must be downloaded. This can be done from the manufacturers website found <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" target="_blank">here</a>. Downloading the tool is very simple; as it is a free tool all that is required is to add Helix3 to the basket and follow the checkout journey. Once this has been completed; the manufacturer E-Fense will then send a link so the file Helix3 can be downloaded. The file to download is an ISO file. Once downloaded this can then be burnt to a disk using the disk burner found in Windows. No further steps are required to be taken to install Helix3.</p>
						</div>
						<div id="pcOnOffTimeInsturctions" class="tab">
							<h3>Instructions</h3>
							<p>PC On/Off Time can be run when running Helix3 in Windows. The tool is located on page 2 in the Incident Response section in Helix3.</p>
							<ol>
								<li>When opened PC On/Off Time loads the live systems event log display the times when the computer was turned on and off as shown in figure 1.<img src="/graphics/poft/main.jpg" class="img"><small>Figure 1 - PC On/Off Time</small></li>
								<li>Dates can be scrolled through for up to 3 weeks ago. However if the system can is changed the results may not be accurate.</li>
								<li>Other computers can also be connected to through the browse option as shown in figure 2.<img src="/graphics/poft/browse.jpg" class="img"><small>Figure 2 - PC On/Off Time Browsing for other computers</small></li>
								<li>Once connected to another computer PC On/Off Time the system log will be ready showing the computers on and off times.</li>
							</ol>
							<p>This concludes the user guide for PC On/Off Time</p>
						</div>
						<div id="pcOnOffTimeOverview" class="tab">
							<h3>Overview</h3>
							<p>Manufacturer - Neuber Software</p>
							<p>Download link - <a href="https://www.e-fense.com/store/index.php?_a=viewProd&productId=11" targte="_blank">www.e-fense.com/store/index.php?_a=viewProd&productId=11</a></p>
							<p>OS - Windows Server, 2000, XP, Vista, 7 and 8.</p>
							<p>Files used - Windows Event Logs</p>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
</div>
<?php include INCLUDES . '/helix3/footer-tags.php'?>
</body>
</html>
