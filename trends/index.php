<?php require_once '../hub.php' ?>
<?php include INCLUDES . 'doctype.php'?>
<head>
<?php include INCLUDES . 'head-tags.php'?>
</head>
<body>
<div id="wrapper">
	<?php include INCLUDES . 'header.php'?>
	<main>
		<section id="hero-unit">
			<div class="content">
				<div id="hero-content">
					<img src="/graphics/profile.jpg" alt="Daniel Stokes" class="profile wow animated fadeInDownBig">
					<h1 class="wow animated fadeInDownBig">Daniel Stokes</h1>
					<h3 class="wow animated fadeInUpBig">Trends I Like</h3>
					<p class="social-icons wow animated fadeInUpBig"><a href="#" class="contact-scroll"><span class="icon icon-email">@</span></a><a href="http://uk.linkedin.com/pub/daniel-stokes/a1/100/786/en" target="_blank"><span class="icon icon-linkedin"></span></a></p>
				</div>
			</div>
		</section>
		<section id="adaptive-placeholders" class="content-section">
			<div class="container">
				<h3>Adaptive Placeholders</h3>
				<div class="col-sm-5">
					<form method="post" action="#" class="ajax-form clearfix" novalidate>
						<div class="form-group">
							<input type="text" name="" class="form-control" required>
							<label placeholder="Adaptive Placeholder form input"></label>
						</div>
						<div class="form-group">
							<textarea name="" class="form-control" required></textarea>
							<label placeholder="Apative Placeholder textarea"></label>
						</div>
					</form>
				</div>
				<div class="col-sm-7">
					<p>One of my favourite trends at the moment is adaptive placeholders. Personally I find it very irritating when filling out a form that uses placeholders instead of labels and want to return to edit a field only to have no idea where the field is.</p>
					<p>Adaptive placeholders are a great solution as they move the placeholder so it's still visible to the user when they have focused on the element or entered valid content.</p>
					<p>Adaptive placeholders can be done purely in CSS when using HTML5 validation and if you don&apos;t want the HTML5 validation to actually run you can add a novalidate attribute to the form element.</p>
				</div>
				<div class="cta col-xs-12">
					<p><a href="#" class="btn contact-scroll">Get in touch!</a></p>
				</div>
			</div>
		</section>
		<section id="animated-svgs" class="content-section blue">
			<div class="container">
				<h3>Animated SVG&apos;s</h3>
				<div class="col-xs-12 col-lg-6">
					<svg width="100%" height="100">
						<circle id="orange-circle" r="30" cx="50" cy="50" fill="white" />
						<animate 
						xlink:href="#orange-circle"
						attributeName="cx"
						from="20%"
						to="80%" 
						dur="1s"
						begin="click"
						fill="freeze" />
					</svg>
				</div>
				<div class="col-xs-12 col-lg-6">
					<p>Although SVG&apos;s have been around for a while now their popularity is only just picking up.</p>
					<p>As SVG&apos;s are vector images they maintain their quality no matter how big it is. On top of this SVG&apos;s can also be animated, in comparison to CSS animations SVG&apos;s allows much more flexibility so things like paths and fills can be used in the animations.</p>
					<p>Click on the circle to test out an animation.</p>
				</div>
				<div class="cta col-xs-12">
					<p><a href="#" class="btn white contact-scroll">Get in touch!</a></p>
				</div>
			</div>
		</section>
		<section id="css-shapes" class="content-section">
			<div class="container">
				<h3>CSS Shapes</h3>
				<div class="col-sm-5">
					<div class="row shapes">
						<div class="col-xs-6 col-sm-4">
							<div class="circle"></div>
						</div>
						<div class="col-xs-6 col-sm-4">
							<div class="triangle"></div>
						</div>
						<div class="col-xs-6 col-sm-4">
							<div class="tear-drop"></div>
						</div>
						<div class="col-xs-6 col-sm-4">
							<div class="star"></div>
						</div>
						<div class="col-xs-6 col-sm-4">
							<div class="hexagon"></div>
						</div>
						<div class="col-xs-6 col-sm-4">
							<div class="pacman"></div>
						</div>
					</div>
				</div>
				<div class="col-sm-7">
					<p>Although it&apos;s not a brand new trend CSS shapes are still being used in new websites as they are an easy to build, style and unlike images maintain their quality on all devices.</p>
					<p>Can be used to create a variety of shapes and most of which can be done by using borders to collapse the side of a shape.</p>
					<p>To demonstrate a variety of CSS shapes I have built some for you to see.</p>
				</div>
				<div class="cta col-xs-12">
					<p><a href="#" class="btn contact-scroll">Get in touch!</a></p>
				</div>
			</div>
		</section>
		<section id="contact" class="content-section blue">
			<div class="container">
				<h3>Get In Touch</h3>
				<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
					<form method="post" action="/ajax/contact.ajax.php" class="ajax-form clearfix wow animated fadeInDown" novalidate>
						<div class="response"></div>
						<input type="hidden" name="check">
						<div class="form-group">
							<input type="text" name="name" class="form-control" required>
							<label placeholder="Name"></label>
						</div>
						<div class="form-group">
							<input type="text" name="email" class="form-control" required>
							<label placeholder="Email"></label>
						</div>
						<div class="form-group">
							<textarea name="message" class="form-control" required></textarea>
							<label placeholder="Message"></label>
						</div>
						<div class="form-group">
							<button type="submit" class="btn white">Send</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</main>
</div>
<?php include INCLUDES . 'footer-tags.php'?>
</body>
</html>