<?php
session_start();

require_once __DIR__ . ('/_site/config.php');

define("INCLUDES", $_SERVER['DOCUMENT_ROOT'] . '/_site/includes/');
require_once  __DIR__ . ('/_hub/functions/hub.function.php');
require_once  __DIR__ . ('/_site/functions/site.function.php');

//Autoloads classes
spl_autoload_register('load_classes');