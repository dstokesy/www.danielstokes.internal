<?php require_once 'hub.php' ?>
<?php include INCLUDES . 'doctype.php'?>
<head>
<?php include INCLUDES . 'head-tags.php'?>
</head>
<body>
<div id="wrapper">
	<?php include INCLUDES . 'header.php'?>
	<main>
		<section id="hero-unit">
			<div class="content">
				<div id="hero-content">
					<img src="/graphics/profile.jpg" alt="Daniel Stokes" class="profile wow animated fadeInDownBig">
					<h1 class="wow animated fadeInDownBig">Daniel Stokes</h1>
					<h3 class="wow animated fadeInUpBig">Web Developer</h3>
					<p class="social-icons wow animated fadeInUpBig"><a href="#" class="contact-scroll"><span class="icon icon-email">@</span></a><a href="http://uk.linkedin.com/pub/daniel-stokes/a1/100/786/en" target="_blank"><span class="icon icon-linkedin"></span></a></p>
				</div>
			</div>
		</section>
		<div id="project-detail"></div>
		<section id="projects" class="content-section">
			<div class="container">
				<h3>Projects</h3>
				<p class="sub-heading">Click on a project to find out more</p>
				<div class="small-container">
					<div class="col-sm-6">
						<article class="project-thumbnail wow animated fadeInLeft">
							<a href="#" class="project-detail-trigger" data-project="tv-reviewer">
								<img src="/graphics/projects/tv-reviewer.jpg" alt="TV Reviewer">
								<div class="project-content">
									<p class="title"><strong>TV Reviewer</strong></p>
									<p class="tag-line">University Project</p>
								</div>
							</a>
						</article>
					</div>
					<div class="col-sm-6">
						<article class="project-thumbnail wow animated fadeInRight">
							<a href="#" class="project-detail-trigger" data-project="case-catalogue">
								<img src="/graphics/projects/case-catalogue.jpg" alt="Case Catalogue">
								<div class="project-content">
									<p class="title"><strong>Case Catalogue</strong></p>
									<p class="tag-line">Case Management</p>
								</div>
							</a>
						</article>
					</div>
					<div class="col-sm-6">
						<article class="project-thumbnail wow animated fadeInLeft">
							<a href="#" class="project-detail-trigger" data-project="helix">
								<img src="/graphics/projects/helix3.jpg" alt="TV Reviewer">
								<div class="project-content">
									<p class="title"><strong>Helix 3</strong></p>
									<p class="tag-line">User guide</p>
								</div>
							</a>
						</article>
					</div>
					<div class="col-sm-6">
						<article class="project-thumbnail wow animated fadeInRight">
							<a href="#" class="project-detail-trigger" data-project="mvc">
								<img src="/graphics/projects/codeigniter.jpg" alt="Case Catalogue">
								<div class="project-content">
									<p class="title"><strong>MVC</strong></p>
									<p class="tag-line">Codeigniter</p>
								</div>
							</a>
						</article>
					</div>
				</div>
				<div class="cta col-xs-12">
					<p class="h3">Like what you see?</p>
					<p><a href="#" class="btn contact-scroll">Get in touch</a></p>
				</div>
			</div>
		</section>
		<section id="skills" class="content-section blue">
			<div class="container">
				<h3>Skills</h3>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInLeft"><span class="icon icon-php"></span>PHP</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInLeft"><span class="icon icon-html5"></span>HTML</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInRight"><span class="icon icon-css3"></span>CSS</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInRight"><span class="icon icon-javascript"></span>JavaScript</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInLeft"><span class="icon icon-mobile"></span>Mobile</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInLeft"><span class="icon icon-share2"></span>Social Media</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInRight"><span class="icon icon-users"></span>Teamwork</p>
				</div>
				<div class="col-xs-6 col-sm-3">
					<p class="skill wow animated fadeInRight"><span class="icon icon-comments"></span>Communication</p>
				</div>
				<div class="cta col-xs-12">
					<p class="h3">Want to know more?</p>
					<p><a href="/bio/" class="btn white">Click here</a></p>
				</div>
			</div>
		</section>
		<section id="contact" class="content-section">
			<div class="container">
				<h3>Get In Touch</h3>
				<div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
					<form method="post" action="/ajax/contact.ajax.php" class="ajax-form clearfix wow animated fadeInDown" novalidate>
						<div class="response"></div>
						<input type="hidden" name="check">
						<div class="form-group">
							<input type="text" name="name" class="form-control" required>
							<label placeholder="Name"></label>
						</div>
						<div class="form-group">
							<input type="text" name="email" class="form-control" required>
							<label placeholder="Email"></label>
						</div>
						<div class="form-group">
							<textarea name="message" class="form-control" required></textarea>
							<label placeholder="Message"></label>
						</div>
						<div class="form-group">
							<button type="submit" class="btn">Send</button>
						</div>
					</form>
				</div>
			</div>
		</section>
	</main>
</div>
<?php include INCLUDES . 'footer-tags.php'?>
</body>
</html>