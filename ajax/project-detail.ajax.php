<div class="container">
	<p class="hide-project-detail-wrapper top"><a href="#" class="hide-project-detail"><span class="icon icon-angle-up"></span> Hide this project <span class="icon icon-angle-up"></span></a></p>
	<?php
	if($_GET['project'] == 'tv-reviewer')
	{
		$project_url = 'http://tv-reviewer.co.uk/';
		?>
		<h1>TV Reviewer</h1>
		<div class="block 1">
			<img src="/graphics/projects/tv-reviewer-detail.jpg" alt="TV Reviewer">
			<div class="content left">
				<p>TV Reviewer was designed to be an easily accessible site. This is maintained by having a fixed navigation bar always present on screen along with large call to action buttons displayed throughout the site.</p>
			</div>
			<div class="content right">
				<p>TV Reviewer demonstrates object orientated programming. The site contains multiple instances of TV Shows. These can be filtered via categories or searched for via their name.</p>
			</div>
		</div>
		<div class="block 2">
			<img src="/graphics/projects/tv-reviewer-detail-3.jpg" alt="TV Reviewer">
			<div class="content left">
				<p>Each TV Show can be loaded individually. These are managed through the content management system on the website.</p>
			</div>
			<div class="content right">
				<p>Users can also register for the site and add TV shows to their watch list and manage their account details.</p>
			</div>
		</div>
		<div class="block 3">
			<img src="/graphics/projects/tv-reviewer-detail-2.jpg" alt="TV Reviewer">
			<div class="content left">
				<p>TV Reviewer also makes use of REST API&apos;s to display the weather. The API is loaded through jQuery. I have also developed my own API so TV Show data can be accessed by others.</p>
			</div>
			<div class="content right">
				<p>Another REST API used is the Facebook API. The API is loaded through PHP in a cron-job and then the data is stored in a database. This data is then loaded in using Object Orientated PHP.</p>
			</div>
		</div>
		<?php
	}
	elseif($_GET['project'] == 'case-catalogue')
	{
		$project_url = 'http://www.casecatalogue.co.uk/';
		?>
		<h1>Case Catalogue</h1>
		<div class="block 1">
			<img src="/graphics/projects/case-catalogue-detail.jpg" alt="Case Catalogue">
			<div class="content left">
				<p>Case Catalogue is a forensic case management system for forensic investigators. The system was built to provide a solution to communication breakdowns between forensic analysts.</p>
			</div>
			<div class="content right">
				<p>The site works on a company basis where a company request to sign up and from then on users can be added to that company and what each user can see and do can be controlled by an administrator for each company.</p>
			</div>
		</div>
		<div class="block 2">
			<img src="/graphics/projects/case-catalogue-detail-2.jpg" alt="Case Catalogue">
			<div class="content left">
				<p>Case Catalogue uses Google's Material Design principals of combining flat design with giving depth to elements to give the impression of 3 dimensions elements on a page.</p>
			</div>
			<div class="content right">
				<p>When each user is logged in their website experience is completely customised to show the data that they are allowed to and want to see.</p>
			</div>
		</div>
		<div class="block 3">
			<img src="/graphics/projects/case-catalogue-detail-3.jpg" alt="Case Catalogue">
			<div class="content left">
				<p>Case Catalogue allows users to enter data on cases, evidence items, case notes and persons of interests. This data is stored within each case and only users that can access or edit this data can do so.</p>
			</div>
			<div class="content right">
				<p>Reporting is also a large part of Case Catalogue allowing administrators to view data from the site and search through this data.</p>
			</div>
		</div>
		<div class="block 4">
			<img src="/graphics/projects/case-catalogue-detail-4.jpg" alt="Case Catalogue">
			<div class="content left">
				<p>Actions taken by a user is also stored in an audit log which is accessible for each company so they can track exactly what each user has done on the website.</p>
			</div>
			<div class="content right">
				<p>The site also includes a notification system which is accessible throughout the entire site can be assigned to users who can also receive the notifications via email.</p>
			</div>
		</div>
		<?php
	}
	elseif($_GET['project'] == 'helix')
	{
		$project_url = 'https://danielstokes.co.uk/helix3/';
		?>
		<h1>Helix 3 Userguide</h1>
		<div class="block 1">
			<img src="/graphics/projects/helix3-detail.jpg" alt="Helix 3 Userguide">
			<div class="content left">
				<p>Helix 3 User guide is one of the first website that I developed and has limited functionality in comparison to my other projects. The site is a user guide for a selection of tools from the Helix 3 computer forensics software suite.</p>
			</div>
			<div class="content right">
				<p>As Helix 3 is an old software suite its design is outdated. To make the website more appealing I chose to use similar colours to Helix 3 to make the web page more modern.</p>
			</div>
		</div>
		<div class="block 2">
			<img src="/graphics/projects/helix3-detail-2.jpg" alt="Helix 3 Userguide">
			<div class="content left">
				<p>As the site required a large amount of content I chose to use tabs to hide content until the user requests it. This also clears up the page making it seem less crowded and more appealing for a user.</p>
			</div>
			<div class="content right">
				<p>Instead of using an existing jQuery tabs plugin I chose to write my own code to toggle between the different content.</p>
			</div>
		</div>
		<?php
	}
	elseif($_GET['project'] == 'mvc')
	{
		$project_url ='https://www.danielstokes.co.uk/codeigniter/';
		?>
		<h1>Codeigniter Site</h1>
		<div class="block 1">
			<img src="/graphics/projects/codeigniter-detail.jpg" alt="Codeigniter site">
			<div class="content left">
				<p>This project demonstrates my ability to use the Model View Controller Codeigniter.</p>
			</div>
			<div class="content right">
				<p>Although this website doesn't demonstrate my best design ability this website was created as a technical demonstration.</p>
			</div>
		</div>
		<div class="block 2">
			<img src="/graphics/projects/codeigniter-detail-2.jpg" alt="Codeigniter site">
			<div class="content left">
				<p>To demonstrate my knowledge of Codeigniter I used TV show models in which I use 2 different views to display all TV Shows and a second to display each show individually.</p>
			</div>
			<div class="content right">
				<p>I also developed a news model. Like the TV Show model news items can be loaded all at once or individually.</p>
			</div>
		</div>
		<div class="block 3">
			<img src="/graphics/projects/codeigniter-detail-3.jpg" alt="Codeigniter site">
			<div class="content left">
				<p>News items can be added if a user is logged in. The login in form makes use the Codeigniter form helper and session checking.</p>
			</div>
			<div class="content right">
				<p>To maintain a consist navigation and styling throughout the website header and navigation views are used instead of include blocks.</p>
			</div>
		</div>
		<?php
	}
	?>
	<div class="cta col-xs-12">
		<p class="h3">Want to see more?</p>
		<p><a href="<?=$project_url?>" class="btn" target="_blank">Visit the site</a></p>
	</div>
	<p class="hide-project-detail-wrapper bottom"><a href="#" class="hide-project-detail"><span class="icon icon-angle-down"></span> Hide this project <span class="icon icon-angle-down"></span></a></p>
</div>