<?php include_once '../hub.php';

if($_POST){
    if($_POST['check'] == "")
    {
        $rsp = array();
        if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
            $error = "Please enter a valid email address";
        elseif($_POST['message'] == "")
            $error = "Please enter an enquiry";
        else
        {
            $message = '
            <table>
                 <tr>
                    <td>Name</td>
                    <td>' . $_POST['name'] . '</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>' . $_POST['email'] . '</td>
                </tr>
                <tr>
                    <td>Enquiry</td>
                    <td>' . $_POST['message'] . '</td>
                </tr>
            </table>
            ';
            $user_message = '
            <table>
                 <tr>
                    <td>Thanks for you message ' . $_POST['name'] . ' I will be in touch soon</td>
                </tr>
            </table>
            ';
            $headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: Daniel Stokes <info@danielstokes.co.uk >' . "\r\n";
            mail('danielstokes93@gmail.com', "Thankyou for your enquiry", $message, $headers);
            mail($_POST['email'], "Thankyou for your enquiry", $user_message, $headers);

            $rsp['response'] = 1;
            $rsp['msg'] = '<div class="success">Thanks for getting in touch</div>';
        }
        if(isset($error))
        {
            $rsp['response'] = 0;
            $rsp['msg'] = '<div class="error">' . $error . '</div>';
        }
        echo json_encode($rsp);
    }
}