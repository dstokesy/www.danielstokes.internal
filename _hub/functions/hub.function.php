<?php
//Include classes
function load_classes($class_name) {
    if(file_exists($_SERVER['DOCUMENT_ROOT']  . "/_hub/classes/" . $class_name . '.class.php')) {
        require_once ($_SERVER['DOCUMENT_ROOT']  . "/_hub/classes/" . $class_name . '.class.php');
    }
    elseif (file_exists($_SERVER['DOCUMENT_ROOT']  . "/_site/classes/" . $class_name . '.class.php')) {
        require_once ($_SERVER['DOCUMENT_ROOT']  . "/_site/classes/" . $class_name . '.class.php');
    }
    else {
        echo $class_name . '.class.php does not exist';
    }
}

//echo prewrap function
function pre_wrap($array)
{
    echo '<pre>';
    print_r($array);
    echo '</pre>';
}

//convert string in to html characters
function html($data)
{
    return htmlentities($data);
}

//echo css function
function echo_css($files)
{
    if($files)
    {
        $less = new lessphp;
        $css_files = array();
        foreach($files as $file)
        {
            $css_file = str_replace("less.css", "css", $file);
            $less->checkedCompile($_SERVER['DOCUMENT_ROOT'] . "/css" . $file, $_SERVER['DOCUMENT_ROOT'] . "/css/compiled" . $css_file); 
            ?><link rel="stylesheet" type="text/css" href="/css/compiled/<?=$css_file?>"/><?php
        }
    }
}

//echo javascript function
function echo_js($files)
{
    if($files)
    {
        foreach($files as $file)
        {
            ?> <script src="/js<?=$file?>"></script> <?php
        }
    }
}

//echo admin css function
function echo_admin_css($files)
{
    if($files)
    {
        $less = new lessphp;
        $css_files = array();
        foreach($files as $file)
        {
            $css_file = str_replace("less.css", "css", $file);
            echo $less->checkedCompile($_SERVER['DOCUMENT_ROOT'] . "/admin/css" . $file, $_SERVER['DOCUMENT_ROOT'] . "/admin/css/compiled" . $css_file);
            ?> <link rel="stylesheet" type="text/css" href="/admin/css/compiled<?=$css_file?>"/> <?php
        }
    }
}

//echo javascript function
function echo_admin_js($files)
{
    if($files)
    {
        foreach($files as $file)
        {
            ?> <script src="/admin/js<?=$file?>"></script> <?php
        }
    }
}

//delete databse record function
function dbRowDelete($table_name, $column, $param)
{
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $db = new PDO("mysql:host=" .DBHOST. "; dbname=" . DBNAME, DBUSER, DBPASS, $options);
    $whereSQL = 'WHERE ' . ':' . $column . ' = ' . $column;
    $sql = "DELETE FROM " . $table_name . ' ' . $whereSQL;
    $query = $db->prepare($sql);
    $query->bindParam(':' . $column, $param, PDO::PARAM_STR);
    return $query->execute();
}

//insert database record function
//dbRowInsert('my_table', $form_data);
function dbRowInsert($table_name, $form_data)
{
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $db = new PDO("mysql:host=" .DBHOST. "; dbname=" . DBNAME, DBUSER, DBPASS, $options);
    $fields = array_keys($form_data);
    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";
    $query = $db->prepare($sql);
    foreach($form_data as $key => $value)
    {
        $query->bindParam(':' . $key, $value, PDO::PARAM_STR);
    }
    return $query->execute();
}

function dbRowUpdate($table_name, $form_data, $where_column, $where_value)
{
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $db = new PDO("mysql:host=" .DBHOST. "; dbname=" . DBNAME, DBUSER, DBPASS, $options);
    $sql = "UPDATE " . $table_name . " SET ";
    $sets = array();
    foreach($form_data as $key => $value)
    {
         $sets[] = $key . ' = :' . $key;
    }
    $sql .= implode(', ', $sets);
    $whereSQL = ' WHERE ' . $where_column . ' = :' . $where_column;
    $sql .= $whereSQL;
    $query = $db->prepare($sql);
    foreach($form_data as $param_key => $param_value)
    {
        $query->bindValue(':' . $param_key, $param_value, PDO::PARAM_STR);
    }
    $query->bindValue(':' . $where_column, $where_value, PDO::PARAM_STR);
    return $query->execute();
}

function dbRowSelect($sql, $params = array(), $return_type = 'FETCH_ASSOC')
{
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $db = new PDO("mysql:host=" .DBHOST. "; dbname=" . DBNAME, DBUSER, DBPASS, $options);
    $query = $db->prepare($sql);
    if(!empty($params))
    {
        foreach($params as $key => $param)
        {
            $query->bindParam($key, $param, PDO::PARAM_INT);    
        }
    }
    $query->execute();

    switch ($return_type) {
        case "FETCH":
            $result = $query->fetchAll();
            return $result;
            break;

        case "FETCH_OBJ":
            $result = $query->fetchAll(PDO::FETCH_OBJ);
            return $result;
            break;

        case "FETCH_ASSOC":
            $result = $query->fetchAll(PDO::FETCH_ASSOC);
            return $result;
            break;

        case "FETCH_BOTH":
            $result = $query->fetchAll(PDO::FETCH_BOTH);
            return $result;
            break;

        case "FETCH_BOUND":
            $result = $query->fetchAll(PDO::FETCH_BOUND);
            return $result;
            break;

        case "FETCH_CLASS":
            $result = $query->fetchAll(PDO::FETCH_CLASS);
            return $result;
            break;

        case "FETCH_LAZY":
            $result = $query->fetch(PDO::FETCH_LAZY);
            return $result;
            break;

        case "FETCH_NAMED":
            $result = $query->fetchAll(PDO::FETCH_NAMED);
            return $result;
            break;

        case "FETCH_NUM":
            $result = $query->fetchAll(PDO::FETCH_NUM);
            return $result;
            break;
        
        default:
            echo "Wrong FETCH METHOD!";
            break;
    }
}

function dbRowSelectOne($sql, $params = array(), $return_type = 'FETCH_ASSOC')
{
    $options = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
    );
    $db = new PDO("mysql:host=" .DBHOST. "; dbname=" . DBNAME, DBUSER, DBPASS, $options);
    $query = $db->prepare($sql);
    if(!empty($params))
    {
        foreach($params as $key => $param)
        {
            $query->bindParam($key, $param, PDO::PARAM_INT);    
        }
    }
    $query->execute();

    switch ($return_type) {
        case "FETCH":
            $result = $query->fetch();
            return $result;
            break;

        case "FETCH_OBJ":
            $result = $query->fetch(PDO::FETCH_OBJ);
            return $result;
            break;

        case "FETCH_ASSOC":
            $result = $query->fetch(PDO::FETCH_ASSOC);
            return $result;
            break;

        case "FETCH_BOTH":
            $result = $query->fetch(PDO::FETCH_BOTH);
            return $result;
            break;

        case "FETCH_BOUND":
            $result = $query->fetch(PDO::FETCH_BOUND);
            return $result;
            break;

        case "FETCH_CLASS":
            $result = $query->fetch(PDO::FETCH_CLASS);
            return $result;
            break;

        case "FETCH_LAZY":
            $result = $query->fetch(PDO::FETCH_LAZY);
            return $result;
            break;

        case "FETCH_NAMED":
            $result = $query->fetch(PDO::FETCH_NAMED);
            return $result;
            break;

        case "FETCH_NUM":
            $result = $query->fetch(PDO::FETCH_NUM);
            return $result;
            break;
        
        default:
            echo "Wrong FETCH METHOD!";
            break;
    }
}

//check email function
function validate_email($email) {
    if (!filter_var($email, FILTER_VALIDATE_EMAIL))
        return false;

    $domain = substr(strrchr($email, "@"), 1);
    if (!checkdnsrr($domain,"MX") || !checkdnsrr($domain,"A"))
        return false;

    return true;
}

function mysql_escape($var) { 
    if(is_array($var)) 
        return array_map(__METHOD__, $var); 

    if(!empty($var) && is_string($var)) { 
        return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $var); 
    } 

    return $var; 
}