<?php
class HTML {

    public static function css($file)
    {
        return '<link href="' . $file . '" rel="stylesheet" type="text/css" />';
    }

    public static function js($file)
    {
        return '<script type="text/javascript" src="' . $file . '"></script>';
    }

}