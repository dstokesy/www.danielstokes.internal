<?php
class User
{

    public function __construct($data)
    {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }

    public static function load($id)
    {
        $result = dbRowSelectOne("SELECT * FROM users WHERE id = :id LIMIT 1", array(':id' => $id));
        return new self($result);
    } 

    public static function load_all()
    {
        $data = dbRowSelect("SELECT * FROM users WHERE live = :live", array(':live' => 1));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    } 

    public function admin_check()
    {
        if($this->admin != 1)
        {
            header('Location: /welcome/');
        }
        else
        {
            return TRUE;
        }
    }

    function get_id()
    {
        return $this->id;
    }

    function get_company()
    {
        return Company::load($this->company_id);
    }

    function get_first_name()
    {
        return $this->first_name;
    }

    function get_surname()
    {
        return $this->surname;
    }

    function get_email()
    {
        return $this->email;
    }

    function get_password()
    {
        return $this->password;
    }

    function get_role()
    {
        return $this->role;
    }

    function get_admin()
    {
        return $this->admin;
    }

    function is_admin()
    {
        return ($this->admin == 1 ? TRUE : FALSE);
    }

    function check_admin()
    {
        if($this->admin == 1)
        {
            return TRUE;
        }
        else
        {
            header('Location: /');
        }
    }

    function get_live()
    {
        return $this->live;
    }

    function get_cases()
    {
        return CaseItem::load_via_user($this->id);
    }

    function get_archived_cases()
    {
        return CaseItem::load_via_user($this->id, TRUE);
    }

    function get_notifications()
    {
        return Notification::load_all();
    }

}