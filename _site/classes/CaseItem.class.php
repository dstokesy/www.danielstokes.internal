<?php
class CaseItem
{

    public function __construct($data)
    {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }

    public static function load($id)
    {
        $result = dbRowSelectOne("SELECT * FROM cases WHERE id = :id LIMIT 1", array(':id' => $id));
        return new self($result);
    } 

    public static function load_all()
    {
        $data = dbRowSelect("SELECT * FROM cases WHERE live = :live", array(':live' => 1));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    }

    public static function load_via_user($user_id, $archived_only = NULL)
    {
        $sql = "
            SELECT c.*
            FROM cases AS c
            INNER JOIN user_case_links as ucl
            ON c.id = ucl.case_id
            WHERE live = :live
            AND ucl.user_id = :user_id";

        if(isset($archived_only))
            $sql .= " AND c.archived = 1";
        else
            $sql .= " AND c.archived = 0";

        $data = dbRowSelect($sql, array(':live' => 1, ':user_id' => $user_id));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    }

    function get_id()
    {
        return $this->id;
    }

    function get_company()
    {
        $company = Company::load($this->company_id);
        return $company;
    }

    function get_name()
    {
        return $this->name;
    }

    function get_description()
    {
        return $this->description;
    }

    function get_status()
    {
        return $this->status;
    }

    function get_created()
    {
        return $this->created;
    }

    function get_archived()
    {
        return $this->archived;
    }

    function get_live()
    {
        return $this->live;
    }

}