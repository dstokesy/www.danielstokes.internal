<?php
class Notification
{

    public function __construct($data)
    {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }

    public static function load($id)
    {
        $result = dbRowSelectOne("SELECT * FROM notifications WHERE id = :id LIMIT 1", array(':id' => $id));
        return new self($result);
    } 

    public static function load_all()
    {
        $data = dbRowSelect("SELECT * FROM notifications WHERE archived = :archived", array(':archived' => 0));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    }

    /*public static function load_via_user($user_id)
    {
        $sql = "
            SELECT n.*, ucl.user_id AS case_user_permission_id
            FROM notifications AS n
            INNER JOIN user_case_links AS ucl
            ON n.case_id = ucl.case_id
            WHERE n.archived = 0";

        $data = dbRowSelect($sql, array(':live' => 1, ':user_id' => $user_id));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    }*/

    function get_id()
    {
        return $this->id;
    }

    function get_case()
    {
        $case = CaseItem::load($this->case_id);
        return $case;
    }

    function get_user()
    {
        $user = User::load($this->user_id);
        return $user;
    }

    function get_title()
    {
        return $this->title;
    }

    function get_content()
    {
        return $this->content;
    }

    function get_archived()
    {
        return $this->archived;
    }

}