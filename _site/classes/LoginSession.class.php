<?php
class LoginSession 
{
  
  private $logged_in=FALSE;
  public $user_id;

  public function __construct($data = NULL)
  {
      if ($data)
      {
          foreach ($data as $key => $value)
          {
              $this->$key = $value;        
          }
      }
  }

  public function is_logged_in()
  {
    return $this->logged_in;
  }

  public function login($user_id)
  {
    if($user_id){
      $_SESSION['user_id'] = $user_id;
    }
  }

  public function logout()
  {
    unset($_SESSION['user_id']);
    unset($this->user_id);
    $this->logged_in = FALSE;
  }

  public function check_login()
  {
    if(isset($_SESSION['user_id'])) {
      $this->user_id = $_SESSION['user_id'];
      $this->logged_in = TRUE;
    } 
    else {
      header('Location: /login/');
    }
  }

  function get_user_id()
  {
    return $this->user_id;
  }

}