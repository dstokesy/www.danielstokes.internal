<?php
class Company 
{

    public function __construct($data)
    {
        if ($data)
        {
            foreach ($data as $key => $value)
            {
                $this->$key = $value;        
            }
        }
    }
    public static function load($id)
    {
        $result = dbRowSelectOne("SELECT * FROM companies WHERE id = :id LIMIT 1", array(':id' => $id));
        return new self($result);
    } 

    public static function load_all()
    {
        $data = dbRowSelect("SELECT * FROM companies WHERE live = :live", array(':live' => 1));

        if ($data)
            foreach ($data as $row)
                $results[] = new self($row);

        return (array) $results;
    } 

    public function admin_check()
    {
        if($this->admin != 1)
        {
            header('Location: /welcome/');
        }
    }

    function get_id()
    {
        return $this->id;
    }

    function get_name()
    {
        return $this->name;
    }

    function get_email()
    {
        return $this->email;
    }

    function get_live()
    {
        return $this->live;
    }

    function check_ip_address()
    {
        if(isset($this->ip_address))
        {
            if($this->ip_address == $_SERVER['REMOTE_ADDR'])
                return TRUE;
            else
                header('Location: /login/?error=access_denied');
        }
        else
        {
            return FALSE;
        }
    }

}