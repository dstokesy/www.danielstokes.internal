<header>
	<div id="navigation-trigger-wrapper">
		<a href="#" id="navigation-trigger" class="popout_trigger clearfix"><span class="hamburger"><span class="top"></span><span class="middle"></span><span class="bottom"></span></span><span class="menu-caption">Menu</span></a>
	</div>
	<div id="navigation">
		<div class="col-xs-12">
			<nav>
				<ul>
					<li><a href="/" <?=($_SERVER['REQUEST_URI'] == '/' ? 'class="active"' : '')?>><span>Home</span></a></li><?php
					?><li><a href="/bio/" <?=($_SERVER['REQUEST_URI'] == '/bio/' ? 'class="active"' : '')?>><span>Bio</span></a></li><?php
					?><li><a href="/trends/" <?=($_SERVER['REQUEST_URI'] == '/trends/' ? 'class="active"' : '')?>><span>Trends</span></a></li><?php
					?><li><a href="#" class="contact-scroll"><span>Contact</span></a></li>
				</ul>
			</nav>
		</div>
	</div>
</header>