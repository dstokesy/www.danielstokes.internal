<?php
echo_js($js = array(
		'/modernizr.js'
	)
);
?>

<!--[if lt IE 9]>
<script type="text/javascript" src="/holding/js/respond.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!--><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script><!--<![endif]-->

<?php
echo_js($js = array(
		'/plugins/wow.min.js',
		'/library/navigation.js',
		'/forms.js',
        '/common.js'
    )
);
