<meta charset="utf-8">
<title>Daniel Stokes - Advanced Internet Development A</title>
<meta name="description" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link href='https://fonts.googleapis.com/css?family=Ubuntu:300,400|Ubuntu+Condensed' rel='stylesheet' type='text/css'>

<?php
echo_css($css = array(
        '/helix3/bootstrap/bootstrap.min.css',
        '/helix3/layout.less.css',
        '/helix3/forms.less.css',
        '/helix3/buttons.less.css',
        '/helix3/icons.less.css',
        '/helix3/library/animate.less.css'
    )
);

echo_js($js = array(
		'/modernizr.js'
	)
);
?>
<!--[if lt IE 9]>
<script type="text/javascript" src="/holding/js/respond.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!--><script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script><!--<![endif]-->
