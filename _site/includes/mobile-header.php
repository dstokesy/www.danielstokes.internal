<div id="header">
	<div id="nav_trigger_wrapper_left" class="popout_trigger_wrapper">
		<a href="#" id="nav_trigger_left" class="popout_trigger clearfix"><span class="hamburger"><span class="top"></span><span class="middle"></span><span class="bottom"></span></span><span class="icon_caption">Menu</span></a>
	</div>
</div>

<div id="popout_left" class="popout">
	<div class="user_details">
		<div class="profile_img"></div>
		
	</div>
	<nav>
		<ul class="nav">
			<li><a href="/">Home</a></li>
			<li><a href="/cases/">Cases</a></li>
			<li><a href="#">Users</a></li>
			<li><a href="/login/">Logout</a></li>
		</ul>
	</nav>
</div>