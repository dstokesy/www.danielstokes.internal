<meta charset="utf-8">
<title>Daniel Stokes</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="/favicon.ico" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

<?php
echo_css($css = array(
        '/bootstrap/bootstrap.min.css',
        '/library/animate.less.css',
        '/icons.less.css',
        '/layout.less.css'
    )
);